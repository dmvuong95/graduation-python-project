from consumer import connection, channel, queueName, exchangeName
import pika

def test_declare_queue():
  resQueue = channel.queue_declare(
    queue=queueName
  )
  assert isinstance(resQueue.method, pika.spec.Queue.DeclareOk), "Can not declare queue"
  print("Test declare queue is OK!")

def test_declare_exchange():
  resExchange = channel.exchange_declare(
    exchange=exchangeName,
    exchange_type="direct"
  )
  assert isinstance(resExchange.method, pika.spec.Exchange.DeclareOk), "Can not declare exchange"
  print("Test declare exchange is OK!")


if __name__ == '__main__':
  test_declare_queue()
  test_declare_exchange()