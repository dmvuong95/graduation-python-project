# Instruction
This will build a docker image base on Ubuntu 16.04, NVIDIA CUDA Toolkit 9.0 and Miniconda 4.9.2 - Python 3.7. To build image, please run the following command:
```bash
$ DOCKER_REGISTRY_HOST=192.168.43.187:5000 sudo docker build -t ${DOCKER_REGISTRY_HOST}/cuda-conda:9.0-py3.7 .
```
