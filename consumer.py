import dotenv
dotenv.load_dotenv()

import os
import pika
import numpy as np
from timeit import default_timer as timer
from numba import vectorize
import functools
import threading

connection = pika.BlockingConnection(
  pika.ConnectionParameters(
    host=os.environ.get('AMQP_HOST'),
    port=os.environ.get('AMQP_PORT'),
    virtual_host=os.environ.get('AMQP_VHOST'),
    heartbeat=5,
    credentials=pika.PlainCredentials(
      username=os.environ.get('AMQP_USERNAME'),
      password=os.environ.get('AMQP_PASSWORD')
    )
  )
)
channel = connection.channel()
channel.basic_qos(prefetch_count=1)
queueName = "my_queue"
exchangeName = "my_exchange"

NUM_ELEMENTS = 200000000
a_source = np.ones(NUM_ELEMENTS, dtype=np.float32)
b_source = np.ones(NUM_ELEMENTS, dtype=np.float32)
# This is the CPU version.
def vector_add_cpu(a, b):
  c = np.zeros(NUM_ELEMENTS, dtype=np.float32)
  for i in range(NUM_ELEMENTS):
    c[i] = a[i] + b[i]
  return c
# This is the GPU version. Note the @vectorize decorator. This tells
# numba to turn this into a GPU vectorized function.
@vectorize(["float32(float32, float32)"], target='cuda')
def vector_add_gpu(a, b):
  return a + b

def ack_msg(ch, delivery_tag):
  if ch.is_open:
    ch.basic_ack(delivery_tag)

def nack_msg(ch, delivery_tag):
  if ch.is_open:
    ch.basic_nack(delivery_tag)

def do_work(
    conn: pika.BlockingConnection,
    channel: pika.adapters.blocking_connection.BlockingChannel,
    delivery_tag,
    data: str):
  try:
    if data == 'cpu':
      # Time the CPU function
      start = timer()
      vector_add_cpu(a_source, b_source)
      vector_add_cpu_time = timer() - start
      print("CPU function took %f seconds." % vector_add_cpu_time)
      pass
    else:
      # Time the GPU function
      start = timer()
      vector_add_gpu(a_source, b_source)
      vector_add_gpu_time = timer() - start
      print("GPU function took %f seconds." % vector_add_gpu_time)
      pass
    conn.add_callback_threadsafe(functools.partial(ack_msg, channel, delivery_tag))
  except Exception as e:
    print('Exception:', e)
    conn.add_callback_threadsafe(functools.partial(nack_msg, channel, delivery_tag))

def on_message(
    channel: pika.adapters.blocking_connection.BlockingChannel,
    method: pika.spec.Basic.Deliver,
    properties: pika.spec.BasicProperties,
    body: bytes, args):
  print(method.delivery_tag, body)
  (conn) = args
  t = threading.Thread(target=do_work, args=(conn, channel, method.delivery_tag, body.decode()))
  t.start()

if __name__ == "__main__":
  resQueue = channel.queue_declare(
    queue=queueName
  )
  # print(resQueue.method.queue)

  resExchange = channel.exchange_declare(
    exchange=exchangeName,
    exchange_type="direct"
  )
  print(resExchange.method)

  channel.queue_bind(
    queue=queueName,
    exchange=exchangeName,
    routing_key="my.routing.key"
  )

  channel.basic_consume(
    resQueue.method.queue, 
    functools.partial(on_message, args=(connection))
  )
  try:
    channel.start_consuming()
    pass
  except KeyboardInterrupt as e:
    print(e)
    channel.stop_consuming()
    connection.close()
    pass
