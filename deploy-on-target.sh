. .env
docker version
echo ${DOCKER_REGISTRY_PASSWORD} | docker login ${DOCKER_REGISTRY_HOST} --username ${DOCKER_REGISTRY_USERNAME} --password-stdin
docker-compose stop
docker-compose rm -f
docker-compose up -d
