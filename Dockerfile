ARG DOCKER_REGISTRY_HOST
FROM ${DOCKER_REGISTRY_HOST}/cuda-conda:9.0-py3.7
ENV AMQP_HOST= AMQP_PORT=5672 AMQP_VHOST=/ AMQP_USERNAME= AMQP_PASSWORD=
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
CMD [ "python", "consumer.py" ]
