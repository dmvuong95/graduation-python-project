import dotenv
dotenv.load_dotenv()

import os
import pika
connection = pika.BlockingConnection(
  pika.ConnectionParameters(
    host=os.environ.get('AMQP_HOST'),
    port=os.environ.get('AMQP_PORT'),
    virtual_host=os.environ.get('AMQP_VHOST'),
    heartbeat=5,
    credentials=pika.PlainCredentials(
      username=os.environ.get('AMQP_USERNAME'),
      password=os.environ.get('AMQP_PASSWORD')
    )
  )
)

channel = connection.channel()
channel.confirm_delivery()

exchangeName = "my_exchange"
resExchange = channel.exchange_declare(
	exchange=exchangeName,
	exchange_type="direct"
)
try:
	channel.basic_publish(
		exchange=exchangeName,
		routing_key="my.routing.key",
		body='gpu',
		properties=pika.BasicProperties(
			content_type="text/plain"
		)
	)
	print('Message publish was confirmed')
except pika.exceptions.UnroutableError as error:
	print('Message could not be confirmed')
